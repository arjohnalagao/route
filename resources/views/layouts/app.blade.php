<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>XPTO</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    {{ __('XPTO') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        @guest
                       
                        @else
                            <a class="nav-link" href="{{ url('/manageRoutes') }}">
                                {{ __('Manage Routes') }}
                            </a>
                        @endguest
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <script>
                setTimeout(function() {
                    $('div.alert').slideUp();
                }, 3000);
            </script>

            <!-- SUCCESS ALERTS -->
            @if(session()->has('addPointSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('addPointSuccess') }}
                </div>
            @endif
            @if(session()->has('updatePointSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('updatePointSuccess') }}
                </div>
            @endif
            @if(session()->has('deletePointSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('deletePointSuccess') }}
                </div>
            @endif
            @if(session()->has('addPoinDesttSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('addPointDestSuccess') }}
                </div>
            @endif
            @if(session()->has('updatePointDestSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('updatePointDestSuccess') }}
                </div>
            @endif
             @if(session()->has('deletePointDestSuccess'))
                <div class="alert alert-success">
                    {{ session()->get('deletePointDestSuccess') }}
                </div>
            @endif



            <!-- FAIL ALERTS -->
            @if(session()->has('addPointFail'))
                <div class="alert alert-danger">
                    {{ session()->get('addPointFail') }}
                </div>
            @endif
            @if(session()->has('updatePointFail'))
                <div class="alert alert-danger">
                    {{ session()->get('updatePointFail') }}
                </div>
            @endif
            @if(session()->has('deletePointFail'))
                <div class="alert alert-danger">
                    {{ session()->get('deletePointFail') }}
                </div>
            @endif
            @if(session()->has('addPointDestFail'))
                <div class="alert alert-danger">
                    {{ session()->get('addPointDestFail') }}
                </div>
            @endif
            @if(session()->has('updatePointDestFail'))
                <div class="alert alert-danger">
                    {{ session()->get('updatePointDestFail') }}
                </div>
            @endif
            @if(session()->has('deletePoinDesttFail'))
                <div class="alert alert-danger">
                    {{ session()->get('deletePointDestFail') }}
                </div>
            @endif

            @yield('content')
        </main>
    </div>
</body>
</html>

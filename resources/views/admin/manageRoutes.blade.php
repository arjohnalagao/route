@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addPoint">Add Point</button>

            <!-- Modal -->
            <div class="modal fade" id="addPoint" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Point</h5>
                            </button>
                        </div>
                        <form method="post" action="{{url('/points.store')}}">
                            @csrf
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="pointName">Point</label>
                                <input type="text" class="form-control" id="pointName" placeholder="Point Name" name="point">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <table class="table table-hover table-sm table-bordered">
                <thead class="thead-light">
                    <tr>
                        <th>Point</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($point_data as $points)
                    <tr>
                        <td width="55%">{{$points->point}}</td>
                        <td width="45%">
                            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#editPoint-{{$points->id}}">Edit</button>
                            <div class="modal fade" id="editPoint-{{$points->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit Point</h5>
                                            </button>
                                        </div>
                                        <form method="post" action="{{url('/points.update/'.$points->id)}}">
                                            @csrf
                                            @method('PUT')
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="pointName">Point</label>
                                                <input type="text" class="form-control" id="pointName" value="{{$points->point}}" name="point">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deletePoint-{{$points->id}}">Delete</button>
                            <div class="modal fade" id="deletePoint-{{$points->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Delete Point</h5>
                                            </button>
                                        </div>
                                        <form method="post" action="{{url('/points.destroy/'.$points->id)}}">
                                            @csrf
                                            @method('PUT')
                                        <div class="modal-body">
                                            Are you sure you want to delete point "{{$points->point}}"?
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                            <button type="submit" class="btn btn-danger">Yes</button>
                                        </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="2" align="center">No Points Yet.</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <div class="col-md-9">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#addPointDest">Add Point Desination</button>

            <!-- Modal -->
            <div class="modal fade" id="addPointDest" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Add Point Destination</h5>
                        </div>

                        <form method="POST" action="{{url('/pointDest.store')}}">
                            @csrf
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="pointDest1" class="col-md-3 col-form-label text-md-right">{{ __('Point Destination') }}</label>
                                <div class="col-md-2">
                                    <select class="form-control" id="pointDest1" name="point_1">
                                        @foreach($point_data as $points)
                                            @if(!empty($points))
                                                <option value="{{$points->point}}">{{$points->point}}</option>
                                            @else
                                                No Available Points
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <label class="col-md-1 col-form-label text-md-right">{{ __('to') }}</label>
                                <div class="col-md-2">
                                    <select class="form-control" id="pointDest2"  name="point_2">
                                        @foreach($point_data as $points)
                                            @if(!empty($points))
                                                <option value="{{$points->point}}">{{$points->point}}</option>
                                            @else
                                                No Available Points
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="days" class="col-md-3 col-form-label text-md-right">{{ __('Days') }}</label>
                                <div class="col-md-3">
                                    <input id="days" type="number" min="1" max="50" class="form-control @error('days') is-invalid @enderror" name="time" required autocomplete="days" placeholder="day/s">
                                    @error('days')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cost" class="col-md-3 col-form-label text-md-right">{{ __('Cost') }}</label>
                                <div class="col-md-3">
                                    <input id="cost" type="text" class="form-control" name="cost" required autocomplete="cost" placeholder="0">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div class="card">
                <div class="card-header">Point Destinations</div>
                <div class="card-body">
                    <table class="table table-hover table-sm">
                        <thead class="thead-light">
                            <tr>
                                <th align="center" width="20%">Point</th>
                                <th align="center" width="10%">to</th>
                                <th align="center" width="20%">Point</th>
                                <th align="center" width="15%">Days</th>
                                <th align="center" width="20%">Cost</th>
                                <th align="center" width="15%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($point_dest_data as $point_dest)
                            <tr>
                                <td>{{$point_dest->point_1}}</td>
                                <td>to</td>
                                <td>{{$point_dest->point_2}}</td>
                                <td>
                                    {{$point_dest->time}}&nbsp;
                                    @if($point_dest->time > 1)
                                    days
                                    @else
                                    day
                                    @endif
                                </td>
                                <td>{{$point_dest->cost}}</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#editPointDest-{{$point_dest->id}}">Edit</button>
                                    <div class="modal fade" id="editPointDest-{{$point_dest->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Edit Point Destination</h5>
                                                    </button>
                                                </div>
                                                <form method="post" action="{{url('/pointDest.update/'.$point_dest->id)}}">
                                                    @csrf
                                                    @method('PUT')
                                                <div class="modal-body">
                                                    <div class="form-group row">
                                                        <label for="pointDest1" class="col-md-3 col-form-label text-md-right">{{ __('Point Destination') }}</label>
                                                        <div class="col-md-2">
                                                            <select class="form-control" id="pointDest1" name="point_1">
                                                                @foreach($point_data as $points)
                                                                    @if(!empty($points))
                                                                        <option value="{{$points->point}}" {{ $points->point == $point_dest->point_1 ? 'selected' : '' }}>{{$points->point}}</option>
                                                                    @else
                                                                        No Available Points
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <label class="col-md-1 col-form-label text-md-right">{{ __('to') }}</label>
                                                        <div class="col-md-2">
                                                            <select class="form-control" id="pointDest2"  name="point_2">
                                                                @foreach($point_data as $points)
                                                                    @if(!empty($points))
                                                                        <option value="{{$points->point}}" {{ $points->point == $point_dest->point_2 ? 'selected' : '' }}>{{$points->point}}</option>
                                                                    @else
                                                                        No Available Points
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="days" class="col-md-3 col-form-label text-md-right">{{ __('Days') }}</label>
                                                        <div class="col-md-3">
                                                            <input id="days" type="number" min="1" max="50" class="form-control @error('days') is-invalid @enderror" name="time" value="{{$point_dest->time}}" required autocomplete="days">
                                                            @error('days')
                                                                <span class="invalid-feedback" role="alert">
                                                                    <strong>{{ $message }}</strong>
                                                                </span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="cost" class="col-md-3 col-form-label text-md-right">{{ __('Cost') }}</label>
                                                        <div class="col-md-3">
                                                            <input id="cost" type="text" class="form-control" name="cost" required autocomplete="cost" value="{{$point_dest->cost}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#deletePointDest-{{$point_dest->id}}">Delete</button>
                                    <div class="modal fade" id="deletePointDest-{{$point_dest->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-md" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Delete Point</h5>
                                                    </button>
                                                </div>
                                                <form method="post" action="{{url('/pointDest.destroy/'.$point_dest->id)}}">
                                                    @csrf
                                                    @method('PUT')
                                                <div class="modal-body">
                                                    Are you sure you want to delete point this path?
                                                    <br>
                                                    Path : {{$point_dest->point_1}}&nbsp;to&nbsp;{{$point_dest->point_2}}
                                                    <br>
                                                    Time : {{$point_dest->time}}&nbsp;
                                                        @if($point_dest->time > 1)
                                                            days
                                                        @else
                                                            day
                                                        @endif
                                                    <br>
                                                    Cost : {{$point_dest->cost}}
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <button type="submit" class="btn btn-danger">Yes</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="6" align="center">No Points Yet.</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/manageRoutes', 'PointsDestinationController@index')->name('mangeRoutes');
Route::post('/points.store', 'PointsController@store');
Route::put('/points.update/{id}', 'PointsController@update');
Route::put('/points.destroy/{id}', 'PointsController@destroy');
Route::post('/pointDest.store', 'PointsDestinationController@store');
Route::put('/pointDest.update/{id}', 'PointsDestinationController@update');
Route::put('/pointDest.destroy/{id}', 'PointsDestinationController@destroy');

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PointsDestination;
use App\Points;
use DB;

class PointsDestinationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $point_data = Points::all();
        $point_dest_data = DB::table('points')
            ->join('points_destinations', function($join)
            {
                $join->on('points.id', '=', 'points_destinations.point_1')->orOn('points.id', '=', 'points_destinations.point_2');
            })
            ->get();
        $point_dest_data = PointsDestination::all();
        return view('/admin/manageRoutes')->with('point_data', $point_data)->with('point_dest_data', $point_dest_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_data = new PointsDestination;
        $new_data->point_1 = $request->point_1;
        $new_data->point_2 = $request->point_2;
        $new_data->time = $request->time;
        $new_data->cost = $request->cost;
        $new_data_save = $new_data->save();

        if($new_data_save){
            return redirect('manageRoutes')->with('addPointDestSuccess', 'Point destination successfully added!');
        } else {
            return redirect('manageRoutes')->with('addPointDestFail', 'Point destination NOT successfully added!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $new_data = PointsDestination::find($id);
        $new_data->point_1 = $request->point_1;
        $new_data->point_2 = $request->point_2;
        $new_data->time = $request->time;
        $new_data->cost = $request->cost;
        $new_data_update = $new_data->save();

        if($new_data_update){
            return redirect('manageRoutes')->with('updatePointDestSuccess', 'Point destination successfully updated!');
        } else {
            return redirect('manageRoutes')->with('updatePointDestFail', 'Point destination NOT successfully updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = PointsDestination::find($id);
        $id_dest_delete = $id->delete();

        if($id_dest_delete){
            return redirect('manageRoutes')->with('deletePointDestSuccess', 'Point destination successfully deleted!');
        } else {
            return redirect('manageRoutes')->with('deletePointDestFail', 'Point destination NOT successfully deleted!');
        }
    }
}

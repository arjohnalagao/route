<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Points;

class PointsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_data = new Points;
        $new_data->point = $request->point;
        $new_data_save = $new_data->save();

        if($new_data_save){
            return redirect('manageRoutes')->with('addPointSuccess', 'Point successfully added!');
        } else {
            return redirect('manageRoutes')->with('addPointFail', 'Point NOT successfully added!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $new_data = Points::find($id);
        $new_data->point = $request->point;
        $new_data_update = $new_data->save();

        if($new_data_update){
            return redirect('manageRoutes')->with('updatePointSuccess', 'Point successfully updated!');
        } else {
            return redirect('manageRoutes')->with('updatePointFail', 'Point NOT successfully updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id = Points::find($id);
        $id_delete = $id->delete();

        if($id_delete){
            return redirect('manageRoutes')->with('deletePointSuccess', 'Point successfully deleted!');
        } else {
            return redirect('manageRoutes')->with('deletePointFail', 'Point NOT successfully deleted!');
        }
    }
}

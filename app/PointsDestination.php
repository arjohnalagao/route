<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PointsDestination extends Model
{
    protected $fillable = [
        'point_1', 'point_2', 'time', 'cost'
    ];
}
